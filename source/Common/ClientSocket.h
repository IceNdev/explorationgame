﻿#pragma once
#include <UnigineStreams.h>

namespace Networking
{
    
class ClientSocket {

public:

    ClientSocket() {}
    /// Client constructor
    ClientSocket(const char* host, const int port, const int recv_size)
    {
        m_recvSize = recv_size;
    
        // creating a UDP socket
        socket = Unigine::Socket::create(Unigine::Socket::SOCKET_DGRAM);

        // opening a socket on the specified port 
        socket->open(host, port);

        // setting the size of the receiving buffer
        socket->recv(m_recvSize);

        // binding the socket to an address figured out from the host used for socket initialization
        socket->bind();

        // setting the socket as a non-blocking one
        socket->nonblock();
    }

    /// Client destructor
    ~ClientSocket()
    {
        // closing the socket
        socket->close();

        // destroying the socket
        socket.deleteLater();
    }

    void setID(int num)
    {
        // setting client's ID
        id = num;
    }

    /// method checking for received packes from the server
    int update()
    {
        // preparing a blob to read the message into
        Unigine::BlobPtr temp_blob = Unigine::Blob::create();
        temp_blob->clear();

        // reading data from the socket
        socket->readStream(temp_blob, m_recvSize);

        if (temp_blob->getSize() > 0){

            // setting current position to start
            temp_blob->seekSet(0);

            // getting client's ID
            int num_client = temp_blob->readShort();

            // checking if the received message is addressed to this particular client and processing it
            if (num_client == id){
                Unigine::Log::message("\nClient[%d] - OPERATION_CODE: %s", id, temp_blob->readLine().get());
            }
        }
        return 1;
    }
private:
    // socket pointer
    Unigine::SocketPtr socket;

    int m_recvSize;

    // client ID
    int id = 0;
};
    
} // Networking

