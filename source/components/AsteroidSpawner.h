﻿#pragma once
#include <tuple>
#include <UnigineComponentSystem.h>
#include <vector>

using namespace Unigine;

class AsteroidSpawner : public Unigine::ComponentBase
{
public:   
    COMPONENT_DEFINE(AsteroidSpawner, ComponentBase)
    COMPONENT_INIT(Init)
    COMPONENT_UPDATE(Update)
    COMPONENT_SHUTDOWN(Shutdown)

    // Parameters
    PROP_PARAM( Float, m_RadiusArea          , 250.0f)
    PROP_PARAM(   Int, m_MaxRandomTries      , 10)
    PROP_PARAM(   Int, m_AsteroidsToGenerate , 80)
    PROP_PARAM( Float, m_MinScale            , 1.0f)
    PROP_PARAM( Float, m_MaxScale            , 8.0f)
    PROP_PARAM(String, m_AsteroidNode        , "nodes/defaultAsteroid.node")
    
protected:
    void Init();
    void Update();
    void Shutdown();
    
private:
    Math::vec3 GeneratePosition(float radius) const;
    Math::vec3 GenerateScale() const;
    void GenerateAsteroids(const float radius, const int asteroids, const int maxTries);
    void SpawnAsteroids();
    void AddAsteroid(const Math::vec3& position, const Math::vec3& scale);
    bool IsPositionValid(const Math::vec3& checkPosition) const;

private:
    // TODO: Change the tuple to a custom data structure (Bruno)
    std::vector<std::tuple<Math::vec3, Math::vec3>> m_Asteroids {};     
    
};
