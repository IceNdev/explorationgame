﻿#include "AsteroidSpawner.h"

#include <tuple>
#include <UnigineInput.h>
#include <UnigineMathLib.h>
#include <UniginePhysics.h>
#include <UnigineProfiler.h>

#include "Random.h"

REGISTER_COMPONENT(AsteroidSpawner)

void AsteroidSpawner::Init()
{
    GenerateAsteroids(m_RadiusArea, m_AsteroidsToGenerate, m_MaxRandomTries);
    SpawnAsteroids();
}

void AsteroidSpawner::Update()
{
    if (Input::isKeyPressed(Input::KEY_F))
    {
        GenerateAsteroids(10000.0f, 80, 10);
        SpawnAsteroids();
    } 
}

void AsteroidSpawner::Shutdown()
{
}

Math::vec3 AsteroidSpawner::GeneratePosition(float radius) const
{    
    Math::vec3 nodePosition = node->getPosition();
    Math::vec3 newPosition = Math::vec3::ZERO;

    Random::Init();
    newPosition.x = Random::Float(nodePosition.x, nodePosition.x + radius);
    newPosition.y = Random::Float(nodePosition.y, nodePosition.y + radius);
    newPosition.z = Random::Float(nodePosition.z, nodePosition.z + radius);

    return newPosition;
}

Math::vec3 AsteroidSpawner::GenerateScale() const
{
    Random::Init();
    const auto newScale = Random::Vec3(m_MinScale, m_MaxScale);
    return newScale;
}

void AsteroidSpawner::GenerateAsteroids(const float radius, const int asteroidsCount, const int maxTries)
{
    for (int i = 0; i < asteroidsCount; ++i)
    {
        const auto scale = GenerateScale();
        for (int tries = 0; tries <= maxTries; ++tries)
        {
            Math::vec3 position = GeneratePosition(radius);            
            if (IsPositionValid(position))
            {
                AddAsteroid(position, scale);
                break;
            }
        }
    }
}

void AsteroidSpawner::SpawnAsteroids()
{
    if (m_Asteroids.empty())
        return;

    for (auto& asteroid : m_Asteroids)
    {
        Math::vec3 indexPosition;
        Math::vec3 indexScale;
        std::tie(indexPosition, indexScale) = asteroid;
        
        NodePtr nodeSpawn = World::loadNode(m_AsteroidNode);
        
        nodeSpawn->setWorldScale(indexScale);
        nodeSpawn->setWorldPosition(indexPosition);
        nodeSpawn->setSaveToWorldEnabled(true);
        
        Math::vec3 getScale = nodeSpawn->getWorldScale();
    }
    
    m_Asteroids.clear();
}

bool AsteroidSpawner::IsPositionValid(const Math::vec3& checkPosition) const
{
    if (m_Asteroids.empty())
        return true;
    for (auto& asteroid : m_Asteroids)
    {
        Math::vec3 indexPosition;
        Math::vec3 indexScale;

        std::tie(indexPosition, indexScale) = asteroid;
        
        if (indexPosition == checkPosition)
            return false;
    }
    return true;
}

void AsteroidSpawner::AddAsteroid(const Math::vec3& position, const Math::vec3& scale)
{
    m_Asteroids.push_back(std::tuple<Math::vec3, Math::vec3> { position, scale});
}
