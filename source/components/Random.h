#pragma once

#include <random>
#include <UnigineCamera.h>

class Random
{
public:
	static void Init()
	{
		s_RandomEngine.seed(std::random_device()());
	}

	// Returns a random number between 0 and 1
	static float Float()
	{
		return static_cast<float>(s_Distribution(s_RandomEngine)) / static_cast<float>(std::numeric_limits<uint32_t>::max());
	}

	// Returns a random number between min value and max value
	static float Float(float min, float max)
	{
		return Float() * (max - min) + min;
	}

	// Returns a random vector with all of the components the same value
	static Unigine::Math::vec3 Vec3(float min, float max)
	{
		Unigine::Math::vec3 vec;
		const float randomNumber = Float(min, max);
		vec.x = randomNumber;
		vec.y = randomNumber;
		vec.z = randomNumber;
		return vec;
	}

private:
	static std::mt19937 s_RandomEngine;
	static std::uniform_int_distribution<std::mt19937::result_type> s_Distribution;
};
