﻿#pragma once

#include <string>
#include <Unigine.h>

using namespace Unigine;

namespace AirTrafficController
{
    
class LandingZone final : public Unigine::ComponentBase
{
public:
    COMPONENT_DEFINE(LandingZone, ComponentBase)
    COMPONENT_INIT(Init)
    COMPONENT_UPDATE(Update)
    COMPONENT_SHUTDOWN(Shutdown)

    // Parameters
    PROP_PARAM(Int, m_size)
    PROP_PARAM(String, m_prefixDebug, "[LZ]:")
    PROP_PARAM(Node, m_displayTextNode)
    PROP_PARAM(Node, m_displayText1Node)

    // Functions
    inline uint32_t GetId() const { return m_Id; }
    inline void SetId(const uint32_t id) { m_Id = id; }
    
    inline uint32_t GetShipId() const { return m_ShipId; }
    inline void SetShipId(const uint32_t id) { m_ShipId = id; }

    inline int GetSize() const { return m_size; }
    inline bool IsActive() const { return m_ShipId != 0u; }

    void LandingZone::SetDisplayText(const std::string text) const;

    inline const Math::vec3& GetPosition() const { return node->getPosition(); }
    inline const Math::quat& GetWorldRotation() const { return node->getWorldRotation(); }

private:
    uint32_t m_Id;
    uint32_t m_ShipId;

protected:
    void Init();
    void Update();
    void Shutdown();

};
    
} // AirTrafficController

