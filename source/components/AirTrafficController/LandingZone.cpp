﻿#include "LandingZone.h"

#include "../Random.h"

namespace AirTrafficController
{
    
REGISTER_COMPONENT(LandingZone)

void LandingZone::Init()
{
    m_ShipId = 0;
    Log::message("%s %i initiated \n", static_cast<const char*>(m_prefixDebug), m_Id);
}

void LandingZone::Update()
{
    // TODO: check collision, if returns true, deactivate the text
}

void LandingZone::Shutdown()
{
}

void LandingZone::SetDisplayText(const std::string text) const
{
    ObjectTextPtr objectText = checked_ptr_cast<ObjectText>(m_displayTextNode.get());
    objectText->setText(text.c_str());
    objectText = checked_ptr_cast<ObjectText>(m_displayText1Node.get());
    objectText->setText(text.c_str());
}
} // AirTrafficController

