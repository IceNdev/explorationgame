﻿#include "Atc.h"

#include "LandingZone.h"
#include "../Random.h"

namespace AirTrafficController
{

REGISTER_COMPONENT(Atc)

void Atc::Init()
{
    InitializeLandingZones();    
}

void Atc::Update()
{
}

void Atc::Shutdown()
{
}

void Atc::InitializeLandingZones()
{
    getComponentsInChildren(m_nodeLandingZones, m_landingZones);

    for (int i = 0; i < m_landingZones.size(); ++i)
    {
        auto lz = m_landingZones.get(i);
        const std::string displayText = String::format("Landing Pad %i", i);
        
        lz->SetId(i);
        lz->SetDisplayText(displayText);
    }
}

LandingZone* Atc::GetLandingZone(const uint32_t landZoneId) const
{
    for (int i = 0; i < m_landingZones.size(); ++i)
    {
        const auto landingZone = m_landingZones.get(i);
        if (landingZone->GetId() == landZoneId)
            return landingZone;
    }
    return nullptr;
}

void Atc::AddShipToLandZone(const uint32_t landZoneId, const uint32_t shipId) const
{
    LandingZone* lz = GetLandingZone(landZoneId);
    if (lz != nullptr)
        lz->SetShipId(1);
}

void Atc::AddShipToLandZone(LandingZone* landingZone, const uint32_t shipId) 
{
    if (landingZone != nullptr)
        landingZone->SetShipId(1);
}

LandingZone* Atc::FindLandZoneAvailable(const uint8_t shipSize) const
{
    for (int i = 0; i < m_landingZones.size(); ++i)
    {
        const auto landingZone = m_landingZones.get(i);
        if (!landingZone->IsActive() && landingZone->GetId() == shipSize)
            return landingZone;
    }
    return nullptr;
}

LandingZone* Atc::AssignShipLandingZone(const uint32_t shipId, const uint8_t shipSize) const
{
    const auto landingZone = FindLandZoneAvailable(shipSize);
    if (landingZone != nullptr)
    {
        AddShipToLandZone(landingZone, shipId);
    }
    return landingZone;
}
    
} // AirTrafficController

