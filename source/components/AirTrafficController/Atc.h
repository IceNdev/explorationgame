﻿#pragma once

#include <Unigine.h>

#include "LandingZone.h"
using namespace Unigine;

namespace AirTrafficController
{
    
class Atc final : public Unigine::ComponentBase
{
public:
    COMPONENT_DEFINE(Atc, ComponentBase)
    COMPONENT_INIT(Init)
    COMPONENT_UPDATE(Update)
    COMPONENT_SHUTDOWN(Shutdown)

    PROP_PARAM(String, m_name, "ATC Floating")
    PROP_PARAM(Node, m_nodeLandingZones)

public:
    const char* GetName() const { return m_name; }
    /**
    Assigns a ship to an available landing zone, and returns the assigned landing zone.
    @param shipId The id of the ship that has requested to land.
    @param shipSize The ship size id.
    @return The assigned landing zone if it's available
    */
    LandingZone* AssignShipLandingZone(const uint32_t shipId, const uint8_t shipSize) const;

private:
    Vector<LandingZone*> m_landingZones;

protected:
    void Init();
    void Update();
    void Shutdown();

private:
    void InitializeLandingZones();
    void AddShipToLandZone(const uint32_t landZoneId, const uint32_t shipId) const;
    static void AddShipToLandZone(LandingZone* landingZone, const uint32_t shipId);
    LandingZone* GetLandingZone(const uint32_t landZoneId) const;
    LandingZone* FindLandZoneAvailable(const uint8_t shipSize) const;

};
    
} // AirTrafficController
