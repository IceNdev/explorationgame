﻿#include "CameraController.h"


#include <UnigineGame.h>
#include <UnigineInput.h>
#include <UnigineVisualizer.h>
#include <UniginePhysics.h>

using namespace Unigine;
using namespace Math;

REGISTER_COMPONENT(CameraController)

void CameraController::Init()
{
    //Unigine::Visualizer::setEnabled(true);
}

void CameraController::Update()
{
    /*
    ivec2 mouse = Input::getMouseCoord();
    float length = 150.0f;
    vec3 start = node->getWorldPosition();
    vec3 end   = start + vec3(Game::getPlayer()->getDirectionFromScreen(mouse.x, mouse.y)) * length;

    int mask = 1;
    WorldIntersectionNormalPtr intersection = WorldIntersectionNormal::create();

    ObjectPtr obj = World::getIntersection(start, end, mask, intersection);

    if (obj)
    {
        vec3 point = intersection->getPoint();
        vec3 normal = intersection->getNormal();
        Visualizer::renderVector(point, point + normal, vec4_blue);
        Log::message("Hit %s at (%f, %f, %f) \n", obj->getName(), point.x, point.y, point.z);
    }*/
}

void CameraController::UpdatePhysics()
{
    

}

void CameraController::PostUpdate()
{
    if (CameraFollow)
    {
        const Unigine::BodyRigidPtr bodyRigid = CameraFollow->getObjectBodyRigid();
        
        node->setWorldPosition(CameraFollow->getWorldPosition());
        node->setWorldRotation(CameraFollow->getWorldRotation());
    }
}

void CameraController::Shutdown()
{
}
