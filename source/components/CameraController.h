﻿#pragma once
#include <UnigineComponentSystem.h>

class CameraController : public Unigine::ComponentBase
{
public:
    
    COMPONENT_DEFINE(CameraController, ComponentBase)
    COMPONENT_INIT(Init)
    COMPONENT_UPDATE(Update)
    COMPONENT_UPDATE_PHYSICS(UpdatePhysics)
    COMPONENT_POST_UPDATE(PostUpdate)
    COMPONENT_SHUTDOWN(Shutdown)

    // Parameters
    PROP_PARAM(Node, CameraFollow)
    
protected:
    void Init();
    void Update();
    void UpdatePhysics();
    void PostUpdate();
    void Shutdown();
};
