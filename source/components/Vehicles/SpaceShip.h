﻿#pragma once

#include <UnigineComponentSystem.h>
#include <UniginePhysics.h>
#include <UniginePlayers.h>
#include <UnigineWidgets.h>

#include "../../Timercpp.h"

using namespace Unigine;

namespace Vehicles
{

class SpaceShip : public Unigine::ComponentBase
{
public:

    COMPONENT_DEFINE(SpaceShip, ComponentBase)
    COMPONENT_INIT(Init);
    COMPONENT_UPDATE(Update);
    COMPONENT_UPDATE_PHYSICS(UpdatePhysics);
    COMPONENT_POST_UPDATE(PostUpdate);
    COMPONENT_SHUTDOWN(Shutdown);

    // Parameters
    PROP_PARAM(Node, RidigBody)

    PROP_PARAM(Node, PlayerNode);
    PROP_PARAM(Int, MouseRotateShip, 1);
    PROP_PARAM(Float, MouseSensitivity, 0.3f);
    PROP_PARAM(Float, Acceleration, 80.0f);
    PROP_PARAM(Float, RollSpeed, 10.0f);
    PROP_PARAM(Float, MaxLinearVelocity, 100.0f);
    PROP_PARAM(Float, Torque, 100.0f);
    PROP_PARAM(Float, ResetSpeed, 0.01f);

    // Sensors
    PROP_PARAM(Float, SensorLength, 150.0f)
    PROP_PARAM(Int,   SensorMultiplier, 100)
    PROP_PARAM(Float, MainSensorPriority, 0.5f)
    PROP_PARAM(Float, SubSensorPriority, 0.25f)
    PROP_PARAM(Float, SubSensorAngle, 15.0f)
    PROP_PARAM(Node, SensorFrontNode);
    PROP_PARAM(Node, SensorBackNode);

    // UI
    PROP_PARAM(Node, ShipInfoTextNode)

protected:
    void Init();
    void Update();
    void UpdatePhysics();
    void PostUpdate();
    
    void Shutdown();

public:
    BodyRigidPtr GetBody() const { return m_spaceBody; }

private:
    // UI
    void InitUserInterface();
    void UserInterfaceVelocities() const;
    void UserInterfaceSensors() const;
    void UserInterfaceRotations() const;
    
    void DisplayShipInfo() const;

    // Sensors
    float GetSensorDistance(NodePtr sensorNode, Math::vec3 direction, float length) const;
    float GetSubSensorDistance(WorldIntersectionNormalPtr intersectionPtr, Math::vec3 startPoint) const;
    void SensorsDetection();

    // Moving
    void ResetVelocities();
    void AxisMoveShip(float& currentVelocity, bool isInput1, bool isInput2) const;
    
    void ApplyForceAtLocation(Math::vec3 force, Math::vec3 position) const;


    // Collisions
    void CollisionCallback(BodyPtr body, int num);

private:
    // Velocities
    float m_currentVelocityForward = 0.0f;
    float m_currentVelocityRight = 0.0f;
    float m_currentVelocityUp = 0.0f;
    float m_velocity = 0.0f;
    bool m_isShipMoving;

    // KeysActions
    bool m_isResetingVelocities = false;

    // Sensors
    float m_sensorFrontDistance;
    float m_sensorBackDistance;

    Math::vec3 m_rotation = Math::vec3::ZERO;
    Math::vec3 m_forward, m_right, m_up;

    // UI
    WidgetLabelPtr m_labelVelocities;
    WidgetLabelPtr m_labelSensors;
    WidgetLabelPtr m_labelRotations;
    WidgetSliderPtr m_sliderSensor;

    GuiPtr m_gui;
    BodyRigidPtr m_spaceBody;
    PlayerPtr m_player;
    ControlsPtr m_controls;
    bool m_isDebugActive;

    ObjectTextPtr m_shipInfoText;

    TimerC m_timer;
};

} // Vehicles

