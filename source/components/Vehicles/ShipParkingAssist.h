﻿#pragma once

#include <Unigine.h>

#include "SpaceShipType.h"
#include "../AirTrafficController/Atc.h"
#include "../AirTrafficController/LandingZone.h"

using namespace Unigine;
namespace Vehicles
{
    
class ShipParkingAssist : public Unigine::ComponentBase
{
public:
    COMPONENT_DEFINE(ShipParkingAssist, ComponentBase)
    COMPONENT_INIT(Init)
    COMPONENT_UPDATE(Update)
    COMPONENT_POST_UPDATE(PostUpdate)
    COMPONENT_SHUTDOWN(Shutdown)

    PROP_PARAM(Node, PlayerNode)
    // TODO: This node needs to be replaced with a trigger
    PROP_PARAM(Node, TempAtc)
    PROP_PARAM(Int, ScoreMultiplier, 2159)
    PROP_PARAM(Int, RotationScoreMultiplier, 10)

    // UI
    PROP_PARAM(Node, ShipAssistTextNode)
    PROP_PARAM(Node, ShipAssistScoreTextNode)

private:
    ControlsPtr m_Controls;
    BodyRigidPtr m_ShipBody;
    SpaceShipType* m_SpaceShipType;
    AirTrafficController::Atc* m_Atc;
    AirTrafficController::LandingZone* m_LandingZone;
    
    bool m_IsAssistActive;
        
    // UI
    WidgetLabelPtr m_LabelTitle;
    WidgetLabelPtr m_LabelLandingZone;
    WidgetLabelPtr m_LabelParkingAssist;
    WidgetWindowPtr m_WindowParkingAssist;
    GuiPtr m_Gui;
    ObjectTextPtr m_ShipAssistText;
    ObjectTextPtr m_ShipAssistScoreText;
    bool m_IsDebugActive;

    // Assists Values
    float m_Distance;
    
    
protected:
    void Init();
    void Update();
    void PostUpdate();
    void Shutdown();

private:
    void InitializeParkingAssist();
    void ParkingUserInterface();
    void RequestLandingZone();
    bool HasRequestedLandingZone() const { return m_LandingZone != nullptr; }
    float GetShipRotationOffset(const Math::vec3 direction, const Math::vec3 axis) const;
    float GetLandingScore() const;
};

} // Vehicles
