﻿#include "SpaceShip.h"

#include <string>
#include <UnigineGame.h>
#include <UnigineMathLib.h>
#include <UniginePhysics.h>
#include <UnigineVisualizer.h>

#include "../../Utils.h"

namespace Unigine {
    class Player;
}

namespace Vehicles
{

REGISTER_COMPONENT(SpaceShip)

void SpaceShip::Init()
{
    if (RidigBody)
    {
        m_spaceBody = RidigBody->getObjectBodyRigid();
        m_spaceBody->setMaxLinearVelocity(MaxLinearVelocity);
    }
    
    m_labelVelocities = nullptr;
    m_labelSensors    = nullptr;
    m_sensorFrontDistance = 0.0f;
    m_sensorBackDistance  = 0.0f;
    m_isDebugActive = true;
    
    if (PlayerNode)
    {
        Log::warning("[Player: %s]", PlayerNode.get()->getName());
        m_player = checked_ptr_cast<Player>(PlayerNode.get());
        m_controls = m_player->getControls();
    }
    m_spaceBody->addContactEnterCallback(MakeCallback(this, &SpaceShip::CollisionCallback));
    
    Unigine::Visualizer::setEnabled(true);
    InitUserInterface();
}

void SpaceShip::Update()
{
    const float dt = Game::getIFps();
    if (m_controls)
    {
        // Keyboard & Mouse
        const bool isMovingFront = m_controls->getState(Controls::STATE_FORWARD) != 0;
        const bool isMovingBack = m_controls->getState(Controls::STATE_BACKWARD) != 0;
        const bool isStrafingLeft = m_controls->getState(Controls::STATE_MOVE_LEFT) != 0;
        const bool isStrafingRight = m_controls->getState(Controls::STATE_MOVE_RIGHT) != 0;
        const bool isStrafingUp = m_controls->getState(Controls::STATE_JUMP) != 0;
        const bool isStrafingDown = m_controls->getState(Controls::STATE_CROUCH) != 0;
        const bool isRotatingLeft = m_controls->getState(Controls::STATE_TURN_LEFT) != 0;
        const bool isRotatingRight = m_controls->getState(Controls::STATE_TURN_RIGHT) != 0;

        m_isShipMoving = isMovingFront || isMovingBack || isStrafingLeft || isStrafingRight || isStrafingUp || isStrafingDown;
        
        AxisMoveShip(m_currentVelocityForward, isMovingBack, isMovingFront);
        AxisMoveShip(m_currentVelocityRight, isStrafingLeft, isStrafingRight);
        AxisMoveShip(m_currentVelocityUp, isStrafingDown, isStrafingUp);

        
        // Rotation
        if(MouseRotateShip)
        {
            m_rotation.z -= m_controls->getMouseDX() * MouseRotateShip * dt;
            m_rotation.x -= m_controls->getMouseDY() * MouseRotateShip * dt;
        }

        // Roll Spaceship
        if (isRotatingLeft)
            m_rotation.y -= RollSpeed * dt;
        else if (isRotatingRight)
            m_rotation.y += RollSpeed * dt;
        
        // Brake-Mode
        if (Input::isKeyDown(Input::KEY_R))
        {
            m_isResetingVelocities = !m_isResetingVelocities;
        }

        SensorsDetection();
    }

    ResetVelocities();

    if (m_spaceBody)
        m_velocity = Math::fsqrt(m_spaceBody->getLinearVelocity().length2());

    
    const Math::quat shipRotation(m_rotation.x, m_rotation.y,  m_rotation.z);
    Math::quat newRotation = m_spaceBody->getRotation() * shipRotation;
    
    newRotation = Math::slerp(m_spaceBody->getRotation(), newRotation, 0.08f);
    m_spaceBody->setRotation(newRotation);

	const Math::quat bodyRotation = m_spaceBody->getRotation();
	m_forward = bodyRotation * Math::vec3_forward;
	m_right = bodyRotation * Math::vec3_right;
	m_up = bodyRotation * Math::vec3_up;
        
}

void SpaceShip::UpdatePhysics()
{
    if (RidigBody)
    {
        m_spaceBody->addForce(m_forward * m_currentVelocityForward);
        m_spaceBody->addForce(m_right * m_currentVelocityRight);
        m_spaceBody->addForce(m_up * m_currentVelocityUp);
    }
}

void SpaceShip::PostUpdate()
{
    if (Input::isKeyDown(Input::KEY_DIGIT_0))
    {
        m_isDebugActive = !m_isDebugActive;

        Utils::EnableUserInterfaceDebug(*m_labelVelocities, m_isDebugActive);
        Utils::EnableUserInterfaceDebug(*m_labelSensors, m_isDebugActive);
        Utils::EnableUserInterfaceDebug(*m_sliderSensor, m_isDebugActive);
        Utils::EnableUserInterfaceDebug(*m_labelRotations, m_isDebugActive);
    }
    
    
    UserInterfaceVelocities();
    UserInterfaceSensors();
    UserInterfaceRotations();
}

void SpaceShip::Shutdown()
{
    m_timer.Stop();
}

void SpaceShip::InitUserInterface()
{
    m_gui = Gui::get();
    m_labelVelocities = WidgetLabel::create(m_gui);
    m_labelVelocities->setPosition(10, 180);
    m_labelVelocities->setFontSize(18);
    m_labelVelocities->setFontOutline(1);

    m_labelSensors = WidgetLabel::create(m_gui);
    m_labelSensors->setPosition(10, 300);
    m_labelSensors->setFontSize(18);
    m_labelSensors->setFontOutline(1);
    
    m_sliderSensor = WidgetSlider::create(m_gui);
    m_sliderSensor->setPosition(200, m_labelSensors->getPositionY());
    m_sliderSensor->setMinValue(7000);
    m_sliderSensor->setMaxValue(0);
    
    m_labelRotations = WidgetLabel::create(m_gui);
    m_labelRotations->setPosition(10, 380);
    m_labelRotations->setFontSize(18);
    m_labelRotations->setFontOutline(1);    

    m_gui->setFont("fonts/VT323-Regular.ttf");
    m_gui->addChild(m_labelVelocities, Gui::ALIGN_OVERLAP);
    m_gui->addChild(m_labelSensors, Gui::ALIGN_OVERLAP);
    m_gui->addChild(m_labelRotations, Gui::ALIGN_OVERLAP);
    m_gui->addChild(m_sliderSensor, Gui::ALIGN_OVERLAP);

    m_shipInfoText = checked_ptr_cast<ObjectText>(ShipInfoTextNode.get());

    
    /*
    timer.SetInterval([&]() {
        DisplayShipInfo();
    }, 150);
    */
    
}

void SpaceShip::UserInterfaceVelocities() const
{
    std::string output = String::format(
        "[DEBUG]\n"
        "Forward Velocity: %f\n"
        "Right Velocity  : %f\n"
        "Up Velocity     : %f\n"
        "Velocity        : %f\n"
        "IsResetting      : %i"
        , m_currentVelocityForward, m_currentVelocityRight, m_currentVelocityUp, m_velocity, m_isResetingVelocities);
        m_labelVelocities->setText(output.c_str());

    DisplayShipInfo();
}

void SpaceShip::UserInterfaceSensors() const
{
    const std::string output = String::format(
        "Sensor Front: %f\n"
        "Sensor Back : %f"
        , m_sensorFrontDistance, m_sensorBackDistance);
    m_labelSensors->setText(output.c_str());

    m_sliderSensor->setValue(static_cast<int>(m_sensorFrontDistance));
}

void SpaceShip::UserInterfaceRotations() const
{
    const auto bodyRotation = m_spaceBody->getRotation();
    const std::string output = String::format(
    "Rotation.X   : %f\n"
    "Rotation.Y   : %f\n"
    "Rotation.Z   : %f\n"
    "Angle X axis : %f"
    , bodyRotation.x, bodyRotation.y, bodyRotation.z, bodyRotation.getAngle(Math::vec3(1.0f, 0.0f, 0.0f)));
    m_labelRotations->setText(output.c_str());
}

void SpaceShip::DisplayShipInfo() const
{
    const int sensorsFront = static_cast<int>(m_sensorFrontDistance);
    const int sensorsBack = static_cast<int>(m_sensorBackDistance);
    m_shipInfoText->setText(String::format(
        "Ship Stats\n"
        "[Speed]\n %.0f m/s \n"
        "[Sensors Front]\n %i u \n"
        "[Sensors Back]\n %i u \n"
        , m_velocity, sensorsFront, sensorsBack));
}

float SpaceShip::GetSensorDistance(NodePtr sensorNode, Math::vec3 direction, float length) const
{
    float sensorAverage = 0.0f;
    
    if (sensorNode)
    {
        //const auto right = spaceBody->getRotation() * Math::vec3::RIGHT;
        const auto startPoint = sensorNode->getWorldPosition();
        const auto endPoint   = startPoint + direction * length;
        const auto endPoint15d = startPoint + ((direction * Math::rotate(m_right, SubSensorAngle)) * length); 
        const auto endPointneg15d = startPoint + ((direction * Math::rotate(m_right, -SubSensorAngle)) * length); 

        auto sensor1 = Utils::RayCast(startPoint, endPoint, 1);
        auto sensor2 = Utils::RayCast(startPoint, endPoint15d, 1);
        auto sensor3 = Utils::RayCast(startPoint, endPointneg15d, 1);

        sensorAverage += GetSubSensorDistance(sensor1, startPoint) * MainSensorPriority;
        sensorAverage += GetSubSensorDistance(sensor2, startPoint) * SubSensorPriority;
        sensorAverage += GetSubSensorDistance(sensor3, startPoint) * SubSensorPriority;
    }
    return sensorAverage / 3;
}

float SpaceShip::GetSubSensorDistance(WorldIntersectionNormalPtr intersectionPtr, Math::vec3 startPoint) const
{
    if (intersectionPtr)
    {
        auto point  = intersectionPtr->getPoint();
        auto normal = intersectionPtr->getNormal();

        Visualizer::renderVector(point, point + normal, Math::vec4_blue);

        return Math::length2(startPoint - point);
    }
    return 0.0f;
}

void SpaceShip::SensorsDetection()
{
    m_sensorFrontDistance = GetSensorDistance(SensorFrontNode, m_forward, SensorLength);
    m_sensorBackDistance  = GetSensorDistance(SensorBackNode, -m_forward, SensorLength);

    m_sensorFrontDistance = m_sensorFrontDistance > 0 ? 1 / m_sensorFrontDistance : m_sensorFrontDistance;
    m_sensorBackDistance = m_sensorBackDistance > 0 ? 1 / m_sensorBackDistance : m_sensorBackDistance;

    m_sensorFrontDistance *= SensorMultiplier;
    m_sensorBackDistance *= SensorMultiplier;
}

void SpaceShip::ResetVelocities()
{
    if (!m_isResetingVelocities)
        return;
    
    m_currentVelocityForward = Math::lerp(m_currentVelocityForward, 0.0f, ResetSpeed);
    m_currentVelocityRight = Math::lerp(m_currentVelocityRight, 0.0f, ResetSpeed);
    m_currentVelocityUp = Math::lerp(m_currentVelocityUp, 0.0f, ResetSpeed);

    m_rotation = Math::lerp(m_rotation, Math::Vec3_zero, ResetSpeed);
}

void SpaceShip::AxisMoveShip(float& currentVelocity, bool isInput1, bool isInput2) const
{
    if (isInput1)
    {
        currentVelocity -= Acceleration;
        
        if (m_velocity < 0.0f)
        {
            currentVelocity = Math::max(currentVelocity, 0.0f);
            currentVelocity += Acceleration;                
        }
    }
    else if (isInput2)
    {
        currentVelocity += Acceleration;
        
        if (m_velocity < 0.0f)
        {
            currentVelocity = Math::min(currentVelocity, 0.0f);
            currentVelocity -= Acceleration;
        }
    }
}

void SpaceShip::ApplyForceAtLocation(Math::vec3 force, Math::vec3 worldPosition) const
{
    m_spaceBody->addForce(force);
    
    const auto worldCenterOfMass = m_spaceBody->getWorldCenterOfMass();
    m_spaceBody->addTorque(Math::cross(worldPosition - worldCenterOfMass, force));

}

void SpaceShip::CollisionCallback(BodyPtr body, int num)
{
    m_currentVelocityForward = 0.0f;
    m_currentVelocityRight = 0.0f;
    m_currentVelocityUp = 0.0f;

    m_rotation = Math::vec3_zero;
}
} // Vehicles


