﻿#include "ShipParkingAssist.h"

#include <string>

#include "SpaceShip.h"
#include "../../Utils.h"
#include "../AirTrafficController/Atc.h"

namespace Vehicles
{
    
REGISTER_COMPONENT(ShipParkingAssist)

void ShipParkingAssist::Init()
{
    Log::message("Parking assist Initialized\n");
    m_Gui = Gui::get();
    m_IsAssistActive = false;
    m_LandingZone = nullptr;
    m_IsDebugActive = true;
    
    m_ShipBody = getComponent<SpaceShip>(node)->GetBody();
    m_SpaceShipType = getComponent<SpaceShipType>(node);
    m_Atc = getComponent<AirTrafficController::Atc>(TempAtc);
    m_ShipAssistText = checked_ptr_cast<ObjectText>(ShipAssistTextNode.get());
    m_ShipAssistScoreText = checked_ptr_cast<ObjectText>(ShipAssistScoreTextNode.get());
    if (PlayerNode)
    {
        m_Controls = checked_ptr_cast<Player>(PlayerNode.get())->getControls();
    }
    
    m_LabelTitle = WidgetLabel::create(m_Gui);
    m_LabelLandingZone = WidgetLabel::create(m_Gui);
    m_LabelParkingAssist = WidgetLabel::create(m_Gui);
    m_WindowParkingAssist = WidgetWindow::create(m_Gui);
    
    if (m_SpaceShipType != nullptr)
        Log::message("ShipId: %i, ShipName: %s, ShipSize: %i\n", m_SpaceShipType->GetShipId(), m_SpaceShipType->GetShipName(), m_SpaceShipType->GetShipSize());
    if (m_Atc != nullptr)
        Log::message("ATCName: %s\n", m_Atc->GetName());

    Visualizer::setEnabled(true);
}

void ShipParkingAssist::Update()
{
    if (Input::isKeyDown(Input::KEY_N))
    {
        m_IsAssistActive = !m_IsAssistActive;
        if (m_IsAssistActive && !HasRequestedLandingZone())
        {
            InitializeParkingAssist();
            RequestLandingZone();
        }
    }
    if (m_Controls)
    {
        const bool hasSubmittedPosition = m_Controls->getState(Controls::STATE_USE) != 0;
        if (hasSubmittedPosition && HasRequestedLandingZone())
        {
            const float score = GetLandingScore();
            m_ShipAssistScoreText->setText(String::format("Score %.4f", score));
        }
    }
}

void ShipParkingAssist::PostUpdate()
{
    if (Input::isKeyDown(Input::KEY_DIGIT_0))
    {
        m_IsDebugActive = !m_IsDebugActive;

        ::Utils::EnableUserInterfaceDebug(*m_WindowParkingAssist, m_IsDebugActive);
    }
    ParkingUserInterface();
}

void ShipParkingAssist::Shutdown()
{
}

void ShipParkingAssist::InitializeParkingAssist()
{
    const auto screenSize = Math::vec2(static_cast<float>(m_Gui->getWidth()), static_cast<float>(m_Gui->getHeight()));
    
    m_LabelTitle->setPosition(0, 0);
    m_LabelTitle->setFontSize(18);
    m_LabelTitle->setFontOutline(0);
    m_LabelTitle->setText("Landing Assist");
    
    m_LabelLandingZone->setPosition(0, 0);
    m_LabelLandingZone->setFontSize(17);
    m_LabelLandingZone->setFontOutline(0);  
    
    m_LabelParkingAssist->setPosition(0, 10);
    m_LabelParkingAssist->setFontSize(17);
    m_LabelParkingAssist->setFontOutline(0);  

    m_WindowParkingAssist->setFontSize(17);
    m_WindowParkingAssist->setFontOutline(1);
    m_WindowParkingAssist->setWidth(300);
    m_WindowParkingAssist->setHeight(200);
    m_WindowParkingAssist->setPosition(static_cast<int>(screenSize.x) - m_WindowParkingAssist->getWidth(), 0);
    //windowParkingAssist->setBackgroundColor(Math::vec4(0.0f, 0.0f, 1.0f, 1.0f));
    m_WindowParkingAssist->setColor(Math::vec4(1.0f, 1.0f, 1.0f, 1.0f));
    m_WindowParkingAssist->addChild(m_LabelTitle);
    m_WindowParkingAssist->addChild(m_LabelLandingZone);
    m_WindowParkingAssist->addChild(m_LabelParkingAssist);
    m_Gui->addChild(m_WindowParkingAssist, Gui::ALIGN_OVERLAP);
}

void ShipParkingAssist::ParkingUserInterface()
{
    std::string strLandingZone;
    std::string strParkingAssist;
    if (m_IsAssistActive)
    {
        if (HasRequestedLandingZone())
        {
            using namespace std;

            const auto landingNode = m_LandingZone->getNode();
            const auto landingTransform = landingNode->getWorldTransform();
       
            float yawAngle   = m_ShipBody->getTransform().getRotate().getAngle(landingTransform.getAxisZ());
            float pitchAngle = m_ShipBody->getTransform().getRotate().getAngle(landingTransform.getAxisX());
            float rollAngle  = m_ShipBody->getTransform().getRotate().getAngle(landingTransform.getAxisY());
        
            // Spaceship alignment with the landing pad. Divide by 180 so we get a number between 0.0 and 1.0
            yawAngle   /= 180.0f;
            pitchAngle /= 180.0f;
            rollAngle  /= 180.0f;

            m_Distance = Math::fsqrt((m_ShipBody->getPosition() - landingNode->getPosition()).length2());
            
            strParkingAssist = String::format(
                "Landing Assist\n"
                "[Yaw] \n %.2f\n"
                "[Pitch] \n %.2f\n"
                "[Roll] \n %.2f\n"
                "[Distance] \n %.1f\n"
                "[Landing Pad] \n Id (%i)",
                yawAngle, pitchAngle, rollAngle, m_Distance, m_LandingZone->GetId());
        }
        else
        {
            strLandingZone = "Landing Zones not available, please try later";
            strParkingAssist = "Landing Assist \n{Offline}";
            m_LabelParkingAssist->setEnabled(false);
        }
    }
    else
    {
        strParkingAssist = "Landing Assist \n{Offline}";
    }
    
    m_LabelLandingZone->setText(strLandingZone.c_str());
    m_LabelParkingAssist->setText(strParkingAssist.c_str());
    m_ShipAssistText->setText(strParkingAssist.c_str());
}

void ShipParkingAssist::RequestLandingZone()
{
    Log::message("Landing zone requested\n");

    const auto landingZone = m_Atc->AssignShipLandingZone(m_SpaceShipType->GetShipId(), m_SpaceShipType->GetShipSize());
    if (landingZone)
    {
        m_LandingZone = landingZone;
    }
}

float ShipParkingAssist::GetShipRotationOffset(const Math::vec3 direction, const Math::vec3 axis) const
{
    return Math::dot(axis, direction);
}

float ShipParkingAssist::GetLandingScore() const
{
    float finalScore = 0.0f;

    // Time spent in the game
    const float time = Game::getTime();

    const auto landingTransform = m_LandingZone->getNode()->getTransform();
    const auto shipTransform    = m_ShipBody->getTransform();

    // Calculate the alignment values
    const float dotPitch = Math::dot(shipTransform.getAxisX(), landingTransform.getAxisX());
    const float dotRoll = Math::dot(shipTransform.getAxisY(), landingTransform.getAxisY());
    const float dotYaw = Math::dot(shipTransform.getAxisZ(), landingTransform.getAxisZ());

    const float rotationScore = dotYaw + dotPitch + dotRoll;
    finalScore = rotationScore + m_Distance + time;
        
    // Invert the result so the higher number is the best score
    finalScore = (1.0f / finalScore) * ScoreMultiplier;
    
    return finalScore;
}
    
} // Vehicles
