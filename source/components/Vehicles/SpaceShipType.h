﻿#pragma once
#include <Unigine.h>

namespace Vehicles
{
    
class SpaceShipType : public Unigine::ComponentBase
{
public:
    COMPONENT_DEFINE(SpaceShipType, ComponentBase)
    
    PROP_PARAM(Int, ShipId, 0)
    PROP_PARAM(String, ShipName)
    PROP_PARAM(Int, ShipSize, 0)
    
    uint32_t GetShipId() const { return static_cast<uint32_t>(ShipId);}
    uint8_t GetShipSize() const { return static_cast<uint8_t>(ShipSize);}
    const char* GetShipName() const { return ShipName;}
};

} // Vehicles
