#pragma once

#include <iostream>
#include <UnigineStreams.h>

#include "ClientSocket.h"

void start_server()
{
    std::cout << "Starting server..." << std::endl;
    // UDP port to be used
    const int UDP_PORT = 8889;

    // UDP receive buffer size 
    const int RECV_SIZE = 7;

    // UDP send buffer size
    const int SEND_SIZE = 7;

    // Max number of pending connections
    const int MAX_PEND_CONNECTIONS = 5;
    
    Unigine::SocketPtr socket;
    Networking::ClientSocket clients[5];
    
    // creating a UDP socket
    socket = Unigine::Socket::create(Unigine::Socket::SOCKET_DGRAM);
		
    // opening a socket on the specified port with a specified broadcast address
    socket->open("127.0.0.1", UDP_PORT);

    // setting the size of the sending buffer
    socket->send(SEND_SIZE);
		
    // setting the socket as a broadcasting one
    socket->broadcast();

    // setting the socket as a non-blocking one
    socket->nonblock();
    std::cout << "Server started" << std::endl;

    while (true)
    {
        const int pendingConnections = socket->listen(MAX_PEND_CONNECTIONS);

        std::cout << "Pending Connections: " << pendingConnections << std::endl;
        if (pendingConnections > 0)
        {
            std::cout << "Connection pending" << std::endl;
            socket->accept(socket);
        }
    }
    
}

int main(int argc, char* argv[])
{
    start_server();
    return 0;
}
