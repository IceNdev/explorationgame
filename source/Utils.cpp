﻿#include "Utils.h"

#include <UnigineVisualizer.h>
#include <UnigineWidgets.h>
#include <UnigineWorld.h>

WorldIntersectionNormalPtr Utils::RayCast(Math::vec3 startPoint, Math::vec3 endPoint, int mask)
{
    WorldIntersectionNormalPtr intersection = WorldIntersectionNormal::create();
    ObjectPtr obj = World::getIntersection(startPoint, endPoint, mask, intersection);

    if (obj)
        endPoint = intersection->getPoint();
    
    Visualizer::renderLine3D(startPoint, endPoint, Math::vec4_red);
    
    return obj ? intersection : nullptr;
}

void Utils::EnableUserInterfaceDebug(Widget& widget, bool status)
{
    widget.setEnabled(!status);
    widget.setHidden(status);
}