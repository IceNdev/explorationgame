﻿#include <iostream>
#include <thread>
#include <chrono>
#include <atomic>

class TimerC {
    std::atomic<bool> active{true};
	
public:

    template<typename Function>
    void SetTimeout(Function function, int delay);

    template<typename Function>
    void SetInterval(Function function, int interval);

    void Stop();

};

template<typename Function>
void TimerC::SetTimeout(Function function, int delay) {
    active = true;
    std::thread t([=]() {
        if(!active.load()) return;
        std::this_thread::sleep_for(std::chrono::milliseconds(delay));
        if(!active.load()) return;
        function();
    });
    t.detach();
}

template<typename Function>
void TimerC::SetInterval(Function function, int interval) {
    active = true;
    std::thread t([=]() {
        while(active.load()) {
            std::this_thread::sleep_for(std::chrono::milliseconds(interval));
            if(!active.load()) return;
            function();
        }
    });
    t.detach();
}

inline void TimerC::Stop() {
    active = false;
}