﻿#include "Test.h"


#include <string>
#include <UnigineGame.h>
#include <UnigineMathLib.h>

REGISTER_COMPONENT(Test);

using namespace Unigine;

void Test::init()
{
    jointWheelFr = nullptr;
    label = nullptr;
    controls = nullptr;
    player = nullptr;
    
    if(WheelFr)
        jointWheelFr = checked_ptr_cast<JointWheel>(WheelFr->getObjectBody()->getJoint(0));
    if(WheelFl)
        jointWheelFl = checked_ptr_cast<JointWheel>(WheelFl->getObjectBody()->getJoint(0));
    if(WheelBr)
        jointWheelBr = checked_ptr_cast<JointWheel>(WheelBr->getObjectBody()->getJoint(0));
    if(WheelBl)
        jointWheelBl = checked_ptr_cast<JointWheel>(WheelBl->getObjectBody()->getJoint(0));
    
    if(PlayerNode)
    {
        Log::warning("[Player: %s]", PlayerNode.get()->getName());
        player = checked_ptr_cast<Player>(PlayerNode.get());
        controls = player->getControls().get();
    }
    
    InitUserInterface();
}

void Test::update()
{
    float dt = Game::getIFps();

    if(controls)
    {
        const bool isMovingFront = controls->getState(Controls::STATE_FORWARD) != 0;
        const bool isMovingBack = controls->getState(Controls::STATE_BACKWARD) != 0;
        const bool isTurningLeft = controls->getState(Controls::STATE_MOVE_LEFT) != 0;
        const bool isTurningRight = controls->getState(Controls::STATE_MOVE_RIGHT) != 0;

        if(isMovingFront)
        {
            currentTorque = DefaultTorque;
            currentVelocity = Math::max(currentVelocity, 0.0f);
            currentVelocity += dt * Acceleration;

            const float rotationZ = node->getRotation().getAngle(Math::vec3::UP);
            
            Log::message("%lf \n", rotationZ);
        }
        else if(isMovingBack)
        {
            currentTorque = DefaultTorque;
            currentVelocity = Math::min(currentVelocity, 0.0f);
            currentVelocity -= dt * Acceleration;
        }
        else
        {
            slowCarDown();
        }

        if (isTurningLeft)
            currentTurnAngle += dt * TurnSpeed;
        else if(isTurningRight)
            currentTurnAngle -= dt * TurnSpeed;
        else
        {
            // Prevent a jitter, because the currentTurnAngle will never be 0
            if(Math::abs(currentTurnAngle) < 0.25f)
                currentTurnAngle = 0.0f;

            // Reset currentTurnAngle to 0
            currentTurnAngle -= Math::sign(currentTurnAngle) * TurnSpeed * dt;
        }
    }

    currentVelocity  = Math::clamp(currentVelocity, -MaxVelocity, MaxVelocity);
    currentTurnAngle = Math::clamp(currentTurnAngle, -MaxTurn, MaxTurn);

    float angle0 = currentTurnAngle;
    float angle1 = currentTurnAngle;

    if (Math::abs(currentTurnAngle) > Math::Consts::EPS)
    {
        float radius  = CarBase / Math::tan(currentTurnAngle * Math::Consts::DEG2RAD);
        float radius0 = radius - CarWidth * 0.5f; 
        float radius1 = radius + CarWidth * 0.5f;

        angle0 = Math::atan(CarBase / radius0) * Math::Consts::RAD2DEG;
        angle1 = Math::atan(CarBase / radius1) * Math::Consts::RAD2DEG;
    }

    if (WheelFl)
        jointWheelFl->setAxis10(Math::rotateZ(angle0).getColumn3(0));
    if (WheelFr)
        jointWheelFr->setAxis10(Math::rotateZ(angle1).getColumn3(0));

    if (RidigBody)
    {
        body = RidigBody->getObjectBodyRigid();
        Log::message("Velocity: %f \n", body->getLinearVelocity().length());
    }
    
    
}

void Test::updatePhysics()
{
    if (AllWheelDrive)
    {
        frontWheelsPower();
        backWheelsPower();
        return;
    }
    if (FrontWheelDrive)
    {
        frontWheelsPower();
        return;
    }
    if (BackWheelDrive)
    {
        backWheelsPower();
        return;
    }
    
}

void Test::shutdown()
{
    
}

void Test::InitUserInterface()
{
    label = WidgetLabel::create(Gui::get());
    label->setPosition(10, 180);
    label->setFontSize(24);
    label->setFontOutline(1);
    Gui::get()->addChild(label, Gui::ALIGN_OVERLAP);
}

void Test::slowCarDown()
{
    const float dt = Game::getIFps();
    currentVelocity *= Math::exp(-dt);    
}

void Test::frontWheelsPower() const
{
    if (WheelFr)
    {
        jointWheelFr->setAngularTorque(currentTorque);
        jointWheelFr->setAngularVelocity(currentVelocity);
    }
    if (WheelFl)
    {
        jointWheelFl->setAngularTorque(currentTorque);
        jointWheelFl->setAngularVelocity(currentVelocity);
    }
}

void Test::backWheelsPower() const
{
    if (WheelBl)
    {
        jointWheelBl->setAngularTorque(currentTorque);
        jointWheelBl->setAngularVelocity(currentVelocity);
    }
    if (WheelBr)
    {
        jointWheelBr->setAngularTorque(currentTorque);
        jointWheelBr->setAngularVelocity(currentVelocity);
    }
}
