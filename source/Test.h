﻿#pragma once
#include <UnigineControls.h>
#include <UniginePhysics.h>
#include <UniginePlayers.h>
#include <UnigineWidgets.h>


#include "UnigineComponentSystem.h"

class Test : public Unigine::ComponentBase
{
public:

    COMPONENT_DEFINE(Test, ComponentBase)

    COMPONENT_INIT(init);
    COMPONENT_UPDATE(update);
    COMPONENT_UPDATE_PHYSICS(updatePhysics);
    COMPONENT_SHUTDOWN(shutdown);

    // Params

    PROP_PARAM(Float, Acceleration, 10.0f)
    PROP_PARAM(Float, MaxVelocity, 50.0f)
    PROP_PARAM(Float, MaxTurn, 25.0f)
    PROP_PARAM(Float, DefaultTorque, 25.0f)
    PROP_PARAM(Float, TurnSpeed, 50.0f)
    PROP_PARAM(Float, CarBase, 3.0f)
    PROP_PARAM(Float, CarWidth, 2.0f)
    PROP_PARAM(Int, AllWheelDrive, true)
    PROP_PARAM(Int, FrontWheelDrive, false)
    PROP_PARAM(Int, BackWheelDrive, false)
    
    PROP_PARAM(String, Name, "name");
    
    PROP_PARAM(Node, WheelFr);
    PROP_PARAM(Node, WheelFl);
    PROP_PARAM(Node, WheelBr);
    PROP_PARAM(Node, WheelBl);
    
    PROP_PARAM(Node, PlayerNode);
    PROP_PARAM(Node, RidigBody)

private:
    void InitUserInterface();
protected:
    // world main loop
    
    void init();
    void update();
    void updatePhysics();
    void shutdown();
    void slowCarDown();
    void frontWheelsPower() const;
    void backWheelsPower() const;
    
private:

    Unigine::JointWheelPtr jointWheelFr;
    Unigine::JointWheelPtr jointWheelFl;
    Unigine::JointWheelPtr jointWheelBr;
    Unigine::JointWheelPtr jointWheelBl;
    
    Unigine::WidgetLabelPtr label;
    Unigine::Controls* controls;
    Unigine::PlayerPtr player;
    Unigine::BodyRigidPtr body;

    float currentVelocity = 0.0f;
    float currentTorque = 0.0f;
    float currentTurnAngle = 0.0f;
};
