﻿#pragma once
#include <UnigineGame.h>
#include <UnigineWorld.h>

using namespace Unigine;

class Utils
{


public:
    // Prefixes
    const char* LandingZonePrefix = "[LZ]";
private:
    static bool m_IsDebugActive;
    
public:
    static WorldIntersectionNormalPtr RayCast(Math::vec3 startPoint, Math::vec3 endPoint, int mask = 0);

    static void EnableUserInterfaceDebug(Widget& widget, bool status);    
};
